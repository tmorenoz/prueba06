<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leads', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('first_name');
            $table->string('last_name');

            $table->unsignedInteger('source_id');
            $table->foreign('source_id')->references('id')->on('leadsources')->onDelete('cascade');
         
            $table->text('leadsourcenames')->nullable();
         
            $table->unsignedInteger('program_id');
            $table->foreign('program_id')->references('id')->on('programs')->onDelete('cascade');

            $table->unsignedInteger('leadstatus_id');
            $table->foreign('leadstatus_id')->references('id')->on('leadstatuses')->onDelete('cascade');
         
            $table->string('phone')->nullable();
            $table->string('mobile')->nullable();
            $table->tinyInteger('credit_report');
            $table->tinyInteger('payment20');
            $table->tinyInteger('payment50');
         
            $table->string('street')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('country')->nullable();
            $table->string('zipcode')->nullable();
            $table->text('description')->nullable();

            $table->Integer('created_by');

            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leads');
    }
}
