<?php

use Illuminate\Database\Seeder;
use App\Taskpriority;
use App\Taskstatus;

class TasksSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Taskpriority::create([
            'value'=>'High'
        ]);
        Taskpriority::create([
            'value'=>'Highest'
        ]);
        Taskpriority::create([
            'value'=>'Low'
        ]);
        Taskpriority::create([
            'value'=>'Lowest'
        ]);
        Taskpriority::create([
            'value'=>'Normal'
        ]);

        //--

        Taskstatus::create([
            'value'=>'Not Started'
        ]);
        Taskstatus::create([
            'value'=>'Deferred'
        ]);
        Taskstatus::create([
            'value'=>'In Progress'
        ]);
        Taskstatus::create([
            'value'=>'Completed'
        ]);
        Taskstatus::create([
            'value'=>'Waiting on someone else'
        ]);

    }
}
