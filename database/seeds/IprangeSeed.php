<?php

use Illuminate\Database\Seeder;
use App\UserIp;

class IprangeSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserIp::create([
            'ip'          => '127.0.0.1',
        ]);

        UserIp::create([
            'ip'          => '192.168.1.1',
        ]);

        UserIp::create([
            'ip'          => '192.255.0.12',
        ]);

        UserIp::create([
            'ip'          => '192.168.15.15',
        ]);
    }
}
