<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(PermissionsTableSeed::class);
         $this->call(UserTableSeed::class);
         $this->call(IprangeSeed::class);
         $this->call(ModulesSeedTable::class);

         $this->call(LeadsSeed::class);
         $this->call(TasksSeed::class);
    }
}
