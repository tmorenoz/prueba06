<?php

use Illuminate\Database\Seeder;
use App\Module;

class ModulesSeedTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Module::create([
            'name' => 'users'
        ]);
        
        Module::create([
            'name' => 'crm'
        ]);

        Module::create([
            'name' => 'business'
        ]);

        Module::create([
            'name' => 'administration'
        ]);

        Module::create([
            'name' => 'dept solution'
        ]);

        Module::create([
            'name' => 'cloud'
        ]);
    }
}
