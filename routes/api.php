<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


// users
Route::apiResources(['user' => 'UserController']);

// my profile
Route::get('profile', 'UserController@profile');

//role
Route::get('role','RoleController@index');

//module
Route::get('module','ModuleController@index');

// leads 
Route::apiResources(['leads'=>'LeadController']);

//notes
Route::apiResources(['notes'=>'NoteController']);


