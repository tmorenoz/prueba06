/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.7.24 : Database - crm_db
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`crm_db` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `crm_db`;

/*Table structure for table `events` */

DROP TABLE IF EXISTS `events`;

CREATE TABLE `events` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `all_day` tinyint(4) DEFAULT NULL,
  `from` date DEFAULT NULL,
  `to` date DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `lead_id` int(10) unsigned NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `events_user_id_foreign` (`user_id`),
  KEY `events_lead_id_foreign` (`lead_id`),
  CONSTRAINT `events_lead_id_foreign` FOREIGN KEY (`lead_id`) REFERENCES `leads` (`id`) ON DELETE CASCADE,
  CONSTRAINT `events_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `events` */

LOCK TABLES `events` WRITE;

insert  into `events`(`id`,`title`,`location`,`all_day`,`from`,`to`,`user_id`,`lead_id`,`description`,`deleted_at`,`created_at`,`updated_at`) values (2,'as','asd',1,'2019-03-09','2019-03-20',1,3,'asd',NULL,'2019-03-01 19:57:38','2019-03-01 19:57:38'),(3,'xxxxxxxxxxxxx','xxxxxxx',NULL,'2019-03-02','2019-03-02',1,1,'adasda',NULL,'2019-03-02 15:41:40','2019-03-05 20:37:03'),(4,'test','ad',NULL,'2019-03-02','2019-03-02',1,2,'asda',NULL,'2019-03-02 15:45:14','2019-03-02 15:45:14'),(5,'x','sa',1,'2019-03-02','2019-03-02',1,2,'as',NULL,'2019-03-02 18:53:43','2019-03-02 18:53:43'),(6,'sad','sad',1,'2019-03-04','2019-03-04',1,4,'dasdasd',NULL,'2019-03-04 18:01:32','2019-03-04 18:01:32'),(7,'xxxx','xxxxx',1,'2019-03-04','2019-03-04',1,5,'sdasdasda',NULL,'2019-03-04 20:57:37','2019-03-04 20:57:37'),(8,'ZXC','XZC',NULL,'2019-03-05','2019-03-05',1,5,'ASDASD',NULL,'2019-03-05 20:35:19','2019-03-05 21:53:06'),(9,'zsdsad','asd',NULL,'2019-03-05','2019-03-05',1,1,'asdasd',NULL,'2019-03-05 20:37:13','2019-03-05 20:37:13'),(10,'title test','California',NULL,'2019-03-05','2019-03-05',2,6,'test of event',NULL,'2019-03-05 22:37:54','2019-03-05 22:37:54'),(11,'xxx','ggggg',NULL,'2019-03-05','2019-03-05',2,3,'gggggggggg',NULL,'2019-03-06 00:56:11','2019-03-06 00:56:11'),(12,'cn','dfdxf',NULL,'2019-04-16','2019-04-16',1,8,'dxfxdf','2019-04-16 21:58:54','2019-04-16 21:57:47','2019-04-16 21:58:54');

UNLOCK TABLES;

/*Table structure for table `iprange` */

DROP TABLE IF EXISTS `iprange`;

CREATE TABLE `iprange` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `iprange` */

LOCK TABLES `iprange` WRITE;

insert  into `iprange`(`id`,`ip`,`created_at`,`updated_at`) values (1,'127.0.0.1','2019-03-01 18:43:36','2019-03-01 18:43:36'),(2,'192.168.1.1','2019-03-01 18:43:36','2019-03-01 18:43:36'),(3,'192.255.0.12','2019-03-01 18:43:36','2019-03-01 18:43:36'),(4,'192.168.15.15','2019-03-01 18:43:36','2019-03-01 18:43:36');

UNLOCK TABLES;

/*Table structure for table `leads` */

DROP TABLE IF EXISTS `leads`;

CREATE TABLE `leads` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `source_id` int(10) unsigned NOT NULL,
  `leadsourcenames` text COLLATE utf8mb4_unicode_ci,
  `program_id` int(10) unsigned NOT NULL,
  `leadstatus_id` int(10) unsigned NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `credit_report` tinyint(4) NOT NULL,
  `payment20` tinyint(4) NOT NULL,
  `payment50` tinyint(4) NOT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zipcode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `leads_user_id_foreign` (`user_id`),
  KEY `leads_source_id_foreign` (`source_id`),
  KEY `leads_program_id_foreign` (`program_id`),
  KEY `leads_leadstatus_id_foreign` (`leadstatus_id`),
  CONSTRAINT `leads_leadstatus_id_foreign` FOREIGN KEY (`leadstatus_id`) REFERENCES `leadstatuses` (`id`) ON DELETE CASCADE,
  CONSTRAINT `leads_program_id_foreign` FOREIGN KEY (`program_id`) REFERENCES `programs` (`id`) ON DELETE CASCADE,
  CONSTRAINT `leads_source_id_foreign` FOREIGN KEY (`source_id`) REFERENCES `leadsources` (`id`) ON DELETE CASCADE,
  CONSTRAINT `leads_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `leads` */

LOCK TABLES `leads` WRITE;

insert  into `leads`(`id`,`user_id`,`first_name`,`last_name`,`source_id`,`leadsourcenames`,`program_id`,`leadstatus_id`,`phone`,`mobile`,`credit_report`,`payment20`,`payment50`,`street`,`city`,`state`,`country`,`zipcode`,`description`,`created_by`,`deleted_at`,`created_at`,`updated_at`) values (1,1,'Maria','Perez',2,'asdasd',1,3,'90909090909','351351',1,1,1,'asda','asda','asd','as',NULL,'asdasdasd',1,NULL,'2019-03-01 18:55:47','2019-03-02 18:33:27'),(2,1,'Joel','Castro',6,'asdasd',3,6,'90909090909','88888888',0,0,0,NULL,NULL,NULL,NULL,NULL,'asdasd',1,NULL,'2019-03-01 19:26:00','2019-03-01 19:26:00'),(3,2,'Daniel','Rodriguez',2,'asdasd',4,3,'123456','351351',0,0,1,'xx','xx','xx','x','xx','xxx',1,NULL,'2019-03-01 19:27:39','2019-03-02 18:38:24'),(4,2,'ferf','lopez',1,'asd',4,4,'898989939','92390239',1,1,0,'asdas','asd','asd','asd','asd','asdasdasd',1,NULL,'2019-03-04 16:30:18','2019-03-04 22:49:19'),(5,2,'asdasd','dsdf',3,'aadas',3,7,'23442343','21234',0,1,0,NULL,NULL,NULL,NULL,NULL,'asdsadsa',1,NULL,'2019-03-04 20:56:43','2019-03-04 22:48:06'),(6,2,'testinh','tst',4,'asdasd',2,9,'90909090909','66666666',1,0,1,'xxxxxxx','x','x','x','x','xx',1,NULL,'2019-03-05 21:55:53','2019-03-05 21:55:53'),(7,1,'Alexis','Sifuentes',1,'asdasd',1,1,'945851240','945851240',1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,'2019-03-07 21:40:54','2019-03-07 21:40:54'),(8,1,'Juan','Perez',1,NULL,2,6,'90909090909','88888888',1,1,1,NULL,NULL,NULL,NULL,NULL,'pooooooo',1,NULL,'2019-04-16 21:51:22','2019-04-16 21:51:22');

UNLOCK TABLES;

/*Table structure for table `leadsources` */

DROP TABLE IF EXISTS `leadsources`;

CREATE TABLE `leadsources` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `leadsources` */

LOCK TABLES `leadsources` WRITE;

insert  into `leadsources`(`id`,`value`,`deleted_at`,`created_at`,`updated_at`) values (1,'Advertisement',NULL,'2019-03-01 18:43:37','2019-03-01 18:43:37'),(2,'Employee Referral',NULL,'2019-03-01 18:43:37','2019-03-01 18:43:37'),(3,'External Referral',NULL,'2019-03-01 18:43:37','2019-03-01 18:43:37'),(4,'Client Referral',NULL,'2019-03-01 18:43:37','2019-03-01 18:43:37'),(5,'Partner',NULL,'2019-03-01 18:43:37','2019-03-01 18:43:37'),(6,'Public Relationship',NULL,'2019-03-01 18:43:37','2019-03-01 18:43:37'),(7,'Chat',NULL,'2019-03-01 18:43:37','2019-03-01 18:43:37');

UNLOCK TABLES;

/*Table structure for table `leadstatuses` */

DROP TABLE IF EXISTS `leadstatuses`;

CREATE TABLE `leadstatuses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `leadstatuses` */

LOCK TABLES `leadstatuses` WRITE;

insert  into `leadstatuses`(`id`,`value`,`deleted_at`,`created_at`,`updated_at`) values (1,'Attempted to Contact',NULL,'2019-03-01 18:43:37','2019-03-01 18:43:37'),(2,'Contact in Future',NULL,'2019-03-01 18:43:37','2019-03-01 18:43:37'),(3,'Contacted',NULL,'2019-03-01 18:43:37','2019-03-01 18:43:37'),(4,'Lost Lead',NULL,'2019-03-01 18:43:37','2019-03-01 18:43:37'),(5,'Not Contacted',NULL,'2019-03-01 18:43:37','2019-03-01 18:43:37'),(6,'Pending',NULL,'2019-03-01 18:43:37','2019-03-01 18:43:37'),(7,'Client',NULL,'2019-03-01 18:43:37','2019-03-01 18:43:37'),(8,'Inactive',NULL,'2019-03-01 18:43:37','2019-03-01 18:43:37'),(9,'Junk Lead',NULL,'2019-03-01 18:43:37','2019-03-01 18:43:37'),(10,'Not Qualified',NULL,'2019-03-01 18:43:37','2019-03-01 18:43:37');

UNLOCK TABLES;

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

LOCK TABLES `migrations` WRITE;

insert  into `migrations`(`id`,`migration`,`batch`) values (86,'2014_10_12_000000_create_users_table',1),(87,'2014_10_12_100000_create_password_resets_table',1),(88,'2015_01_20_084450_create_roles_table',1),(89,'2015_01_20_084525_create_role_user_table',1),(90,'2015_01_24_080208_create_permissions_table',1),(91,'2015_01_24_080433_create_permission_role_table',1),(92,'2015_12_04_003040_add_special_role_column',1),(93,'2017_10_17_170735_create_permission_user_table',1),(94,'2019_02_19_213906_create_iprange_table',1),(95,'2019_02_20_201354_create_modules_table',1),(96,'2019_02_20_201602_add_field_role_user',1),(97,'2019_02_25_173237_create_leadsources_table',1),(98,'2019_02_25_173448_create_programs_table',1),(99,'2019_02_25_173511_create_leadstatuses_table',1),(100,'2019_02_25_173536_create_leads_table',1),(101,'2019_02_25_173549_create_notes_table',1),(102,'2019_02_28_172051_create_taskstatuses_table',1),(103,'2019_02_28_172137_create_taskpriorities_table',1),(104,'2019_02_28_172211_create_tasks_table',1),(105,'2019_03_01_001726_create_events_table',1),(106,'2019_03_01_002028_create_participants_table',1);

UNLOCK TABLES;

/*Table structure for table `modules` */

DROP TABLE IF EXISTS `modules`;

CREATE TABLE `modules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `modules` */

LOCK TABLES `modules` WRITE;

insert  into `modules`(`id`,`name`,`deleted_at`,`created_at`,`updated_at`) values (1,'users',NULL,'2019-03-01 18:43:36','2019-03-01 18:43:36'),(2,'crm',NULL,'2019-03-01 18:43:36','2019-03-01 18:43:36'),(3,'business',NULL,'2019-03-01 18:43:36','2019-03-01 18:43:36'),(4,'administration',NULL,'2019-03-01 18:43:36','2019-03-01 18:43:36'),(5,'dept solution',NULL,'2019-03-01 18:43:36','2019-03-01 18:43:36'),(6,'cloud',NULL,'2019-03-01 18:43:37','2019-03-01 18:43:37');

UNLOCK TABLES;

/*Table structure for table `notes` */

DROP TABLE IF EXISTS `notes`;

CREATE TABLE `notes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `lead_id` int(10) unsigned NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notes_user_id_foreign` (`user_id`),
  KEY `notes_lead_id_foreign` (`lead_id`),
  CONSTRAINT `notes_lead_id_foreign` FOREIGN KEY (`lead_id`) REFERENCES `leads` (`id`) ON DELETE CASCADE,
  CONSTRAINT `notes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `notes` */

LOCK TABLES `notes` WRITE;

insert  into `notes`(`id`,`user_id`,`lead_id`,`text`,`deleted_at`,`created_at`,`updated_at`) values (1,1,3,'asdasdsadasd',NULL,'2019-03-01 21:14:21','2019-03-01 21:14:21'),(2,1,3,'fsfdfcx',NULL,'2019-03-01 21:17:36','2019-03-01 21:17:36'),(6,1,3,'coma',NULL,'2019-03-01 22:18:38','2019-03-01 22:18:38'),(7,1,3,'nueva nota agregada',NULL,'2019-03-01 23:05:04','2019-03-01 23:05:04'),(8,1,3,'x',NULL,'2019-03-01 23:14:47','2019-03-01 23:14:47'),(9,1,3,'s',NULL,'2019-03-01 23:14:51','2019-03-01 23:14:51'),(10,1,3,'sasds',NULL,'2019-03-01 23:15:01','2019-03-01 23:15:01'),(11,1,3,'xczxc',NULL,'2019-03-01 23:15:05','2019-03-01 23:15:05'),(12,1,1,'prueba',NULL,'2019-03-02 15:29:54','2019-03-02 15:29:54'),(13,1,2,'xxxxxxxx',NULL,'2019-03-02 18:56:00','2019-03-02 18:56:00'),(14,1,2,'probando',NULL,'2019-03-02 19:13:31','2019-03-02 19:13:31'),(15,1,1,'test note add',NULL,'2019-03-04 16:49:48','2019-03-04 16:49:48'),(16,1,1,'test data note two',NULL,'2019-03-04 16:50:11','2019-03-04 16:50:11'),(17,1,1,'test data note three',NULL,'2019-03-04 16:50:26','2019-03-04 16:50:26'),(18,1,4,'sasdasdas',NULL,'2019-03-04 17:54:29','2019-03-04 17:54:29'),(19,1,4,'asdasdasd',NULL,'2019-03-04 18:01:38','2019-03-04 18:01:38'),(20,1,3,'testing',NULL,'2019-03-04 20:36:57','2019-03-04 20:36:57'),(21,1,4,'testing datax',NULL,'2019-03-04 20:40:01','2019-03-04 20:40:01'),(22,1,4,'testeando',NULL,'2019-03-04 20:47:52','2019-03-04 20:47:52'),(23,1,5,'asdasds',NULL,'2019-03-04 20:57:57','2019-03-04 20:57:57'),(24,1,5,'asdasdas',NULL,'2019-03-04 20:58:08','2019-03-04 20:58:08'),(25,1,5,'xxxxxx',NULL,'2019-03-05 21:52:52','2019-03-05 21:52:52'),(26,1,6,'xxxxxxxx',NULL,'2019-03-05 21:58:01','2019-03-05 21:58:01'),(27,1,6,'xxxxxxzzzzzzzz',NULL,'2019-03-05 23:52:53','2019-03-05 23:52:53'),(28,1,8,'vghvghvghvgh',NULL,'2019-04-16 22:01:32','2019-04-16 22:01:32'),(29,1,8,'vghgvhgvhvhvg',NULL,'2019-04-16 22:01:37','2019-04-16 22:01:37'),(30,1,8,'vghvghvgh',NULL,'2019-04-16 22:01:41','2019-04-16 22:01:41');

UNLOCK TABLES;

/*Table structure for table `participants` */

DROP TABLE IF EXISTS `participants`;

CREATE TABLE `participants` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `event_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `participants_event_id_foreign` (`event_id`),
  KEY `participants_user_id_foreign` (`user_id`),
  CONSTRAINT `participants_event_id_foreign` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE CASCADE,
  CONSTRAINT `participants_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `participants` */

LOCK TABLES `participants` WRITE;

insert  into `participants`(`id`,`event_id`,`user_id`,`deleted_at`,`created_at`,`updated_at`) values (1,2,1,NULL,NULL,NULL),(2,2,2,NULL,NULL,NULL),(3,3,2,NULL,NULL,NULL),(4,6,2,NULL,NULL,NULL),(5,7,1,NULL,NULL,NULL),(6,8,1,NULL,NULL,NULL),(7,9,1,NULL,NULL,NULL),(8,10,1,NULL,NULL,NULL),(9,11,1,NULL,NULL,NULL);

UNLOCK TABLES;

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

LOCK TABLES `password_resets` WRITE;

UNLOCK TABLES;

/*Table structure for table `permission_role` */

DROP TABLE IF EXISTS `permission_role`;

CREATE TABLE `permission_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `permission_role` */

LOCK TABLES `permission_role` WRITE;

insert  into `permission_role`(`id`,`permission_id`,`role_id`,`created_at`,`updated_at`) values (1,1,4,NULL,NULL),(2,1,1,NULL,NULL),(3,2,1,NULL,NULL),(4,3,1,NULL,NULL),(5,4,1,NULL,NULL);

UNLOCK TABLES;

/*Table structure for table `permission_user` */

DROP TABLE IF EXISTS `permission_user`;

CREATE TABLE `permission_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permission_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permission_user_permission_id_index` (`permission_id`),
  KEY `permission_user_user_id_index` (`user_id`),
  CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `permission_user` */

LOCK TABLES `permission_user` WRITE;

UNLOCK TABLES;

/*Table structure for table `permissions` */

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `permissions` */

LOCK TABLES `permissions` WRITE;

insert  into `permissions`(`id`,`name`,`slug`,`description`,`created_at`,`updated_at`) values (1,'Views data','data.view','Lista y navega todos los usuarios del sistema','2019-03-01 18:43:35','2019-03-01 18:43:35'),(2,'Create data','data.create','Ve en detalle cada usuario del sistema','2019-03-01 18:43:35','2019-03-01 18:43:35'),(3,'Edit data','data.edit','Podría editar cualquier dato de un usuario del sistema','2019-03-01 18:43:35','2019-03-01 18:43:35'),(4,'Delete data','data.delete','Podría eliminar cualquier usuario del sistema','2019-03-01 18:43:36','2019-03-01 18:43:36');

UNLOCK TABLES;

/*Table structure for table `programs` */

DROP TABLE IF EXISTS `programs`;

CREATE TABLE `programs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `programs` */

LOCK TABLES `programs` WRITE;

insert  into `programs`(`id`,`value`,`deleted_at`,`created_at`,`updated_at`) values (1,'Business',NULL,'2019-03-01 18:43:37','2019-03-01 18:43:37'),(2,'Boost Credit',NULL,'2019-03-01 18:43:37','2019-03-01 18:43:37'),(3,'Credit Repair',NULL,'2019-03-01 18:43:37','2019-03-01 18:43:37'),(4,'Debt Solution',NULL,'2019-03-01 18:43:37','2019-03-01 18:43:37'),(5,'Tax Research',NULL,'2019-03-01 18:43:37','2019-03-01 18:43:37');

UNLOCK TABLES;

/*Table structure for table `role_user` */

DROP TABLE IF EXISTS `role_user`;

CREATE TABLE `role_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `module_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `role_user_role_id_index` (`role_id`),
  KEY `role_user_user_id_index` (`user_id`),
  KEY `role_user_module_id_index` (`module_id`),
  CONSTRAINT `role_user_module_id_foreign` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `role_user` */

LOCK TABLES `role_user` WRITE;

insert  into `role_user`(`id`,`role_id`,`user_id`,`created_at`,`updated_at`,`module_id`) values (1,2,2,NULL,NULL,1),(2,3,2,NULL,NULL,2),(7,2,3,NULL,NULL,2),(8,3,3,NULL,NULL,6);

UNLOCK TABLES;

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `special` enum('all-access','no-access') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`),
  UNIQUE KEY `roles_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `roles` */

LOCK TABLES `roles` WRITE;

insert  into `roles`(`id`,`name`,`slug`,`description`,`created_at`,`updated_at`,`special`) values (1,'Administrator','adm',NULL,'2019-03-01 18:43:36','2019-03-01 18:43:36','all-access'),(2,'Supervisor','superv',NULL,'2019-03-01 18:43:36','2019-03-01 18:43:36','all-access'),(3,'Adviser','adv',NULL,'2019-03-01 18:43:36','2019-03-01 18:43:36','all-access'),(4,'Operator','ope',NULL,'2019-03-01 18:43:36','2019-03-01 18:43:36','all-access');

UNLOCK TABLES;

/*Table structure for table `taskpriorities` */

DROP TABLE IF EXISTS `taskpriorities`;

CREATE TABLE `taskpriorities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `taskpriorities` */

LOCK TABLES `taskpriorities` WRITE;

insert  into `taskpriorities`(`id`,`value`,`created_at`,`updated_at`) values (1,'High','2019-03-01 18:43:38','2019-03-01 18:43:38'),(2,'Highest','2019-03-01 18:43:38','2019-03-01 18:43:38'),(3,'Low','2019-03-01 18:43:38','2019-03-01 18:43:38'),(4,'Lowest','2019-03-01 18:43:38','2019-03-01 18:43:38'),(5,'Normal','2019-03-01 18:43:38','2019-03-01 18:43:38');

UNLOCK TABLES;

/*Table structure for table `tasks` */

DROP TABLE IF EXISTS `tasks`;

CREATE TABLE `tasks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_created` date NOT NULL,
  `lead_id` int(10) unsigned NOT NULL,
  `taskstatuses_id` int(10) unsigned NOT NULL,
  `taskpriorities_id` int(10) unsigned NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tasks_user_id_foreign` (`user_id`),
  KEY `tasks_lead_id_foreign` (`lead_id`),
  KEY `tasks_taskstatuses_id_foreign` (`taskstatuses_id`),
  KEY `tasks_taskpriorities_id_foreign` (`taskpriorities_id`),
  CONSTRAINT `tasks_lead_id_foreign` FOREIGN KEY (`lead_id`) REFERENCES `leads` (`id`) ON DELETE CASCADE,
  CONSTRAINT `tasks_taskpriorities_id_foreign` FOREIGN KEY (`taskpriorities_id`) REFERENCES `taskpriorities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `tasks_taskstatuses_id_foreign` FOREIGN KEY (`taskstatuses_id`) REFERENCES `taskstatuses` (`id`) ON DELETE CASCADE,
  CONSTRAINT `tasks_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `tasks` */

LOCK TABLES `tasks` WRITE;

insert  into `tasks`(`id`,`user_id`,`subject`,`date_created`,`lead_id`,`taskstatuses_id`,`taskpriorities_id`,`description`,`deleted_at`,`created_at`,`updated_at`) values (1,1,'asasdas','2019-03-12',1,1,1,'asd',NULL,'2019-03-01 18:57:42','2019-03-01 18:57:42'),(2,1,'asasdas','2019-02-26',1,2,2,'sa',NULL,'2019-03-01 19:00:46','2019-03-05 20:31:56'),(3,2,'asasdas','2019-03-13',1,4,4,'assa',NULL,'2019-03-01 19:04:40','2019-03-05 20:29:01'),(4,2,'asasdas','2019-03-20',1,2,2,'yh',NULL,'2019-03-01 19:16:33','2019-03-05 20:31:38'),(5,2,'asasdas','2019-02-28',3,3,3,'sac',NULL,'2019-03-01 19:35:34','2019-03-01 19:35:34'),(6,1,'test','2019-03-02',2,3,4,'testeando',NULL,'2019-03-02 15:45:39','2019-03-02 15:45:39'),(7,1,'asasdas','2019-03-04',4,1,1,'asdas',NULL,'2019-03-04 18:01:19','2019-03-04 18:01:19'),(8,1,'aaaaa','2019-03-04',5,5,4,'adasdasd',NULL,'2019-03-04 20:57:13','2019-03-05 17:00:47'),(9,2,'asasdas','2019-03-05',5,1,2,'jh',NULL,'2019-03-05 20:30:25','2019-03-05 20:30:29'),(10,2,'asasdas','2019-03-05',4,4,1,'s',NULL,'2019-03-05 20:33:06','2019-03-05 20:33:11'),(11,1,'asasdas','2019-03-05',6,3,2,'xxxxxxx',NULL,'2019-03-05 22:37:27','2019-03-05 22:37:27'),(12,1,'xxxxxxx','2019-03-06',3,3,1,'xxxxx xxxxx',NULL,'2019-03-06 00:55:51','2019-03-06 00:55:51');

UNLOCK TABLES;

/*Table structure for table `taskstatuses` */

DROP TABLE IF EXISTS `taskstatuses`;

CREATE TABLE `taskstatuses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `taskstatuses` */

LOCK TABLES `taskstatuses` WRITE;

insert  into `taskstatuses`(`id`,`value`,`created_at`,`updated_at`) values (1,'Not Started','2019-03-01 18:43:38','2019-03-01 18:43:38'),(2,'Deferred','2019-03-01 18:43:38','2019-03-01 18:43:38'),(3,'In Progress','2019-03-01 18:43:38','2019-03-01 18:43:38'),(4,'Completed','2019-03-01 18:43:38','2019-03-01 18:43:38'),(5,'Waiting on someone else','2019-03-01 18:43:38','2019-03-01 18:43:38');

UNLOCK TABLES;

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cellphone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `restriction_by_ip` tinyint(4) NOT NULL DEFAULT '0',
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

LOCK TABLES `users` WRITE;

insert  into `users`(`id`,`first_name`,`last_name`,`email`,`email_verified_at`,`password`,`phone`,`cellphone`,`address`,`date_of_birth`,`restriction_by_ip`,`status`,`remember_token`,`created_at`,`updated_at`,`deleted_at`) values (1,'Roger','Segura','administrador@amg.com',NULL,'$2y$10$INTq.dougEEmUpyg7HxhLeJmwrzPUwa.ZYCnz5kbDFpdIEBMtcc0W','+519999999','00000000','San Juan de Luringancho, Lima, Peru','1984-12-23',0,'1','P91uDUmno7N6bCyxDnxVbcANEUx6V6cDDs65AL35MLq4SPy1ncYJ66lV0pih','2019-03-01 18:43:36','2019-04-16 21:09:08',NULL),(2,'Test','user','test@amg.com',NULL,'$2y$10$ITOzPZY6x49HpvBCoOE1VuBuA3WY48ErjZfBaDztVmWSKSTZDn8V2','+519999999','00000000','San Juan de Luringancho, Lima, Peru','1984-12-23',0,'1',NULL,'2019-03-01 18:43:36','2019-03-04 18:24:27',NULL),(3,'Tamy','Moreno','developer.soft.amg@outlook.com',NULL,'$2y$10$INTq.dougEEmUpyg7HxhLeJmwrzPUwa.ZYCnz5kbDFpdIEBMtcc0W','56432211','987757141','Jr. Puno 345','2019-04-16',0,'1',NULL,'2019-04-16 21:30:17','2019-04-16 21:37:41','2019-04-16 21:37:41');

UNLOCK TABLES;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
