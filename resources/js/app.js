

require('./bootstrap');

window.Vue = require('vue');

import VueRouter from 'vue-router'
import { Form, HasError, AlertError } from 'vform'
import moment from 'moment'
import VueProgressBar from 'vue-progressbar'
import Swal from 'sweetalert2'

import Gate from "./Gate";

import vSelect from 'vue-select'

Vue.component('v-select', vSelect)

require('vue2-animate/dist/vue2-animate.min.css')

Vue.prototype.$gate = new Gate(window.user);


window.swal = Swal;

const toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
  });

window.toast = toast;
window.Form = Form;

Vue.use(VueProgressBar, {
    color: 'rgb(143, 255, 199)',
    failedColor: 'red',
    height: '3px'
  })

Vue.use(VueRouter)

// routes
let routes = [
    // users
    
    { path: '/users', component: require('./components/users/User.vue').default },
    { path: '/profiles', component: require('./components/users/Profile.vue').default },
    { path: '/details-profile', component: require('./components/users/Details.vue').default },
    // crm
    { path: '/crm/feeds', component: require('./components/crm/Feeds.vue').default },
    { path: '/crm/calendar', component: require('./components/crm/Calendar.vue').default },
    { path: '/crm/leads', component: require('./components/crm/Leads.vue').default },
    { path: '/crm/user/list', component: require('./components/crm/UserList.vue').default },   

    // business

    // not found page
    { path: '*', component: require('./components/NotFound.vue').default }
]

const router = new VueRouter({
    routes, 
    mode: 'history'
  })





// Components
//Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component(HasError.name, HasError).default
Vue.component(AlertError.name, AlertError).default
Vue.component('pagination', require('laravel-vue-pagination'));

// filters
Vue.filter('myDate',function(created){
    return moment(created).format('MMMM Do YYYY');
});

window.Fire =  new Vue();



/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    router,
    data:{
        search: ''
    },
    methods:{
        searchit: _.debounce(() => {
            Fire.$emit('searching');
        },1000),

        printme() {
            window.print();
        }
    }
});
