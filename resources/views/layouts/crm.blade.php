<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="stylesheet" href="/css/app.css">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper" id="app">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
     
    </ul>

    <!-- SEARCH FORM -->
    <div class="input-group input-group-sm">
        
      </div>

           <!-- Right navbar links -->
           <ul class="navbar-nav ml-auto">
            <!-- Messages Dropdown Menu -->
            
            <!-- Notifications Dropdown Menu -->
            <li class="nav-item dropdown">
              <a class="nav-link" data-toggle="dropdown" href="#">
                <i class="fa fa-cog"></i>
                <span class="badge badge-info navbar-badge">MODULE: CRM</span>
              </a>
            
            </li>
           
          </ul>
   
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src="{{ asset('images/amgsoft.png') }}" alt="AMGSOFT Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text black font-weight-light">AMG SOFT</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
        <img src="{{ asset('images/avatar.png') }}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block black">{{ Auth::user()->first_name }}</a>
            
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

               <li class="nav-item">
                <a href="/home" class="nav-link">
                  <i class="nav-icon fas fa-home black"></i>
                  <p>
                    Back to Panel
                    
                  </p>
                </a>
              </li>

              <li class="nav-item">
                <router-link to="/crm/feeds" class="nav-link">
                  <i class="nav-icon fas fa-users black"></i>
                  <p>
                    Feeds
                    
                  </p>
                </router-link>
              </li>

              <li class="nav-item">
                  <router-link to="/crm/calendar" class="nav-link">
                  <i class="nav-icon fas fa-calendar black"></i>
                  <p>
                    Calendar
                    
                  </p>
                </router-link>
              </li>

              <li class="nav-item">
                <router-link to="/crm/leads" class="nav-link">
                <i class="nav-icon fas fa-user-tag black"></i>
                <p>
                  Leads
                  
                </p>
              </router-link>
            </li>

            
            <li class="nav-item">
                <router-link to="/crm/user/list" class="nav-link">
                <i class="nav-icon fas fa-user-tag black"></i>
                <p>
                  User list
                  
                </p>
              </router-link>
            </li>

              
              <li class="nav-item">
                  <a class="nav-link" href="{{ route('logout') }}"
                      onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                      <i class="nav-icon fas fa-power-off red"></i>
                      <p>{{ __('Logout') }}</p>
                  </a>

                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                  </form>

              
              </li>

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
   
    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
            
          <transition name="custom-classes-transition"
          enter-active-class="animated bounceInLeft"
          leave-active-class="animated bounceOutLeft fast" mode="out-in">
                    <router-view></router-view>

          </transition>

          <vue-progress-bar></vue-progress-bar>
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->



  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; {{date('Y')}} <a href="#">AMGSOFT</a>.</strong> All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<script src="/js/app.js"></script>
@auth
<script>
    window.user = @json(auth()->user())
</script>
@endauth
</body>
</html>