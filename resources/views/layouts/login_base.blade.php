<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>

  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

 <link rel="stylesheet" href="{{asset('css/app.css')}}">

 <style>
        .loginbody 
                {
                        background-image: url('../images/background.jpg');
                     
                        background-position: center; /* Center the image */
                        background-repeat: no-repeat; /* Do not repeat the image */
                        background-size: cover; /* Resize the background image to cover the entire container */;
                }
 </style>
</head>
<body class="hold-transition login-page loginbody " id="app" >

     
        {{-- id="test" <-- test is from resources/views/sass/app.scss effect opacity --}}
        <div class="login-box mt-5" id="test">
                        @yield('content')
        </div>
   
<!-- js -->
<script src="{{asset('js/app.js')}}"></script>

</body>
</html>