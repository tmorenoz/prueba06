<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="stylesheet" href="/css/app.css">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper" id="app">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
     
    </ul>

       <!-- Right navbar links -->
       <ul class="navbar-nav ml-auto">
        <!-- Messages Dropdown Menu -->
        
        <!-- Notifications Dropdown Menu -->
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#">
            <i class="fa fa-cog"></i>
            <span class="badge badge-info navbar-badge">MODULE: MY PROFILE</span>
          </a>
        
        </li>
       
      </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src="{{ asset('images/amgsoft.png') }}" alt="AMGSOFT Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text black font-weight-light">AMG SOFT</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
        <img src="{{ asset('images/avatar.png') }}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block black">{{ Auth::user()->first_name }}</a>
            
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

               <li class="nav-item">
                <a href="/home" class="nav-link">
                  <i class="nav-icon fas fa-home black"></i>
                  <p>
                    Back to Panel
                    
                  </p>
                </a>
              </li>

              <li class="nav-item">
              <a class="nav-link active" href="{{ url('/my-profile') }}">
                  <i class="nav-icon fas fa-user-tag black"></i>
                  <p>
                    Profile
                    
                  </p>
                </a>
              </li>

             


              
              <li class="nav-item">
                  <a class="nav-link" href="{{ route('logout') }}"
                      onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                      <i class="nav-icon fas fa-power-off red"></i>
                      <p>{{ __('Logout') }}</p>
                  </a>

                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                  </form>

              
              </li>

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
   
    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
            
        <div class="container">
            <div class="row">

                <div class="col-md-12">
                        @if($errors->any())
                        <ul class="alert alert-danger"> 
                        @foreach ($errors->all() as $error)
                                <li >{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif 
                </div>
                   
                <div class="col-md-12 mt-3">
                    <div class="card card-widget widget-user">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header text-black box-level-modal">
                        <h3 class="widget-user-username">Profile: {{ $user->first_name }} {{ $user->last_name }}</h3>
                        <h5 class="widget-user-desc">
                            @foreach($user->roles as $rol)
                                <p>{{ $rol->name }}</p>
                            @endforeach
                        </h5>
                    </div>
                    <div class="widget-user-image">
                       
                    </div>
                    
                    </div>
                </div>
    
                <!-- tab -->
    
                <div class="col-md-12">
                    <div class="card  box-level-modal">
                        <div class="card-header p-2" style="display:none;">
                            <ul class="nav nav-pills">
                            <li class="nav-item"><a class="nav-link" href="#activity" data-toggle="tab">Activity</a></li>
                            <li class="nav-item"><a class="nav-link active show" href="#settings" data-toggle="tab">Settings</a></li>
                            </ul>
                        </div><!-- /.card-header -->
                        <div class="card-body">
                            <div class="tab-content">
                                <!-- Activity Tab -->
                                <div class="tab-pane" id="activity">
                                    <h3 class="text-center">Display User Activity</h3>
                                </div>
                                <!-- Setting Tab -->
                                <div class="tab-pane active show" id="settings">
                                    <form class="form-horizontal" action="{{ route('users.update', $user->id) }}" method="POST">
                                        @csrf
                                        {{ method_field('PUT') }}
                                    
                                        <div class="row">
                                              <div class="col-md-6">
                                                  <div class="form-group">
                                                      <label for="inputName" class="col-sm-3 control-label{{ $errors->has('first_name') ? ' is-invalid' : '' }}">First Name</label>
                                                      <div class="col-sm-12">
                                                          <input type="text" name="first_name" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" id="first_name" placeholder="First name" value="{{ $user->first_name }}">
                                                      </div>
                                                         
                                                  </div>
                                              </div>
                                              <div class="col-md-6">
                                                  <div class="form-group">
                                                      <label for="inputExperience" class="col-sm-3 control-label{{ $errors->has('last_name') ? ' is-invalid' : '' }}">Last name</label>
                                                      <div class="col-sm-12">
                                                          <input type="text" name="last_name"  class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" id="last_name" placeholder="Last name" value="{{ $user->last_name }}">
                                                        </div>
                                                                
                                                                
                                                       </div>
                                              </div>
                                        </div>
                                       

                                          <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="inputEmail" class="col-sm-3 control-label">Email</label>
                                                    <div class="col-sm-12">
                                                        <input type="email" disabled name="email"  class="form-control" id="inputEmail" placeholder="Email" value="{{ $user->email }}">
                                                    </div>
                                                       
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="inputCellphone" class="col-sm-3 control-label">Date of Birth</label>
                                                    <div class="col-sm-12">
                                                        <input type="date" name="date_of_birth"  class="form-control" id="inputdate_of_birth" placeholder="date_of_birth" value="{{ $user->date_of_birth }}">
                                                    </div>
                                                </div>
                                            </div>
                                           
                                          </div>

                                      <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="inputPhone" class="col-sm-3 control-label">Phone</label>
                                                <div class="col-sm-12">
                                                    <input type="text" maxlength="13" name="phone"  class="form-control" id="inputPhone" placeholder="Phone" value="{{ $user->phone }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="inputCellphone" class="col-sm-3 control-label">Cellphone</label>
                                                <div class="col-sm-12">
                                                    <input type="text" maxlength="13" name="cellphone"  class="form-control" id="inputCellphone" placeholder="cellphone" value="{{ $user->cellphone }}">
                                                </div>
                                            </div>
                                        </div>
                                       

                                       
                                      </div>

                                           <div class="row">
                                              <div class="col-md-6">
                                                  <div class="form-group">
                                                      <label for="password" class="col-sm-12 control-label">Password (leave empty if not changing)</label>
                                                      <div class="col-sm-12">
                                                          <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                                                      </div>
                                                      @if ($errors->has('password'))
                                                          <span class="invalid-feedback" role="alert">
                                                              <strong>{{ $errors->first('password') }}</strong>
                                                          </span>
                                                      @endif
                                                  </div>
                                              </div>
                                              <div class="col-md-6">
                                                  <div class="form-group">
                                                      <label for="inputAddress" class="col-sm-2 control-label">Address</label>
                                                      <div class="col-sm-12">
                                                          <textarea  name="address" class="form-control" id="inputAddress" placeholder="Address">{{ $user->address }}</textarea>
                                                      </div>
                                                  </div>    
                                              </div>
                                           </div>

                                              
    
                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-12">
                                        <button  type="submit" class="btn btn-primary">Update</button>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            <!-- /.tab-pane -->
                            </div>
                            <!-- /.tab-content -->
                        </div><!-- /.card-body -->
                    </div>
                    <!-- /.nav-tabs-custom -->
              </div>
              <!-- end tabs -->
            </div>
        </div>
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->



  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; {{date('Y')}} <a href="#">AMGSOFT</a>.</strong> All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<script src="/js/app.js"></script>
@auth
<script>
    window.user = @json(auth()->user())
</script>
@endauth
</body>
</html>