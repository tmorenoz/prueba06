@extends('layouts.login_base')

@section('content')

<div class="card">
    <div class="card-body login-card-body">
                <div class="login-box-msg">
                    <p class="alert alert-danger">{{ __('Unauthorized access for AMG SOFT') }}</p>
                    <p><em>Please comunicate with administrator</em></p>
                </div>
    </div>
</div>
@endsection