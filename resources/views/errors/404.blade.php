@extends('layouts.login_base')

@section('content')

<div class="card">
    <div class="card-body login-card-body">
                <div class="login-box-msg">
                    <p class="alert alert-danger">{{ __('Page not Found AMG SOFT') }}</p>
                </div>
                <p class="mb-1">
                            <a class="btn btn-link" href="{{ route('/') }}">
                                {{ __('Forgot Your Password?') }}
                            </a>
                </p>
    </div>
</div>
@endsection