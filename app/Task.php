<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; 

use App\User;
use App\Taskpriority;
use App\Lead;
use App\Taskstatus;

class Task extends Model
{
    use SoftDeletes;

    protected $table ='tasks';

    protected $fillable = [
        'user_id',
        'subject',
        'date_created',
        'lead_id',
        'taskstatuses_id',
        'taskpriorities_id',
        'description'
    ];

    protected $dates = ['deleted_at']; 

    public function users(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function lead(){
        return $this->belongsTo(Lead::class, 'lead_id');
    }

    public function status(){
        return $this->belongsTo(Taskstatus::class, 'taskstatuses_id');
    }

    public function priority(){
        return $this->belongsTo(Taskpriority::class, 'taskpriorities_id');
    }
}
