<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Caffeinated\Shinobi\Traits\ShinobiTrait;
use Illuminate\Database\Eloquent\SoftDeletes; 

use App\Module;
class User extends Authenticatable
{
    use Notifiable, ShinobiTrait, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
         'email',
          'password',
           'phone',
            'cellphone',
             'address',
              'date_of_birth',
               'restriction_by_ip',
               'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = ['deleted_at']; 

    public function module()
    {
        return $this->belongsToMany(Module::class,'role_user');
    }

    // scopes for filter data

    public function scopeRol($query, $search){
        if($search)
            return $query->where('first_name','LIKE',"%$search%")
                                ->orWhere('last_name','LIKE',"%$search%")
                                    ->orWhere('email','LIKE',"%$search%")
                                        ->orWhereHas('roles', function($s) use ($search) {
                                            $s->where('name','LIKE',"%$search%");
                                        }) 
                                            ->orWhereHas('module', function($s) use ($search) {
                                                $s->where('name','LIKE',"%$search%");
                                            });
    }

}
