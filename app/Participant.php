<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Event;

class Participant extends Model
{
    protected $table ='participants';

    protected $fillable = [
        'event_id',
        'user_id'
    ];


    public function events(){
        return $this->belongsToMany(Event::class, 'participants', 'event_id', 'user_id');
    }

    public function users(){
        return $this->belongsTo(User::class,'user_id');
    }
}
