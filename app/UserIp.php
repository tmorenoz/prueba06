<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserIp extends Model
{
    protected $table = 'iprange';

    protected $fillable = ['ip'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
