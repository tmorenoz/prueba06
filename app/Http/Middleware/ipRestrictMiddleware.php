<?php

namespace App\Http\Middleware;

use Closure;
use App\UserIp;
use Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth as IlluminateAuth;


class ipRestrictMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // middleware get ip's user for control access by ip.
        
        $restricted_ip = UserIp::all();
        $ipreceipt = $request->ip();

        if(Auth::user()->restriction_by_ip == 1)
        {
            
           $dato = $restricted_ip->pluck('ip');
         
           $comparation = in_array($ipreceipt, $dato->toArray());
           
           if(!$comparation)
           {
                Log::info('user block access for limit ip, address ip: ' . $ipreceipt);
                Auth::logout();
                return redirect('/login')->with('error','Unauthorized access for AMG SOFT');
           }
              
        }

        return $next($request);
    }
}
