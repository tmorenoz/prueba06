<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function users()
    {
        // views module users
        return view('home');
    }

    public function crm()
    {
         // views module crm
        return view('layouts.crm');
    }

    public function panel()
    {
         // views module panel initial
        return view('welcome');
    }

    
}
