<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Lead;
use App\Leadsource;
use App\Program;
use App\User;
use App\Leadstatus;

use Illuminate\Support\Facades\Auth;

class LeadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // leads relationships with user, status.... file app\Lead.php

        return Lead::with(['user','status','createby','source','program'])->latest()->paginate(5);
    }

    public function getData(){
        // return data for create or edit leads 
        $leadsource = Leadsource::all();
        $program = Program::all();
        $owner = User::all();
        $leadstatus = Leadstatus::all();

        return response()->json([
            'leadsource' => $leadsource,
            'program' => $program,
            'owner' => $owner,
            'leadstatus' => $leadstatus
        ]);

    }

 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // return validate before create lead new 

        $this->validate($request, [
            'first_name' => 'required|string|max:191',
            'last_name' => 'required|string|max:191',
            'user_id' => 'required',
            'source_id' => 'required',
            'credit_report' => 'required',
            'leadstatus_id' => 'required',

        ]);

            $userid = Auth::id();
       
         $leads = Lead::create([
            'user_id' => $request['user_id'],
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'source_id' => $request['source_id'],
            'leadsourcenames' => $request['leadsourcenames'],
            'program_id' => $request['program_id'],
            'phone' => $request['phone'],
            'mobile' => $request['mobile'],
            'credit_report' => $request['credit_report'],
            'payment20' => $request['payment20'],
            'payment50' => $request['payment50'],
            'leadstatus_id' => $request['leadstatus_id'],
            'street' => $request['street'],
            'city' => $request['city'],
            'state' => $request['state'],
            'country' => $request['country'],
            'zipcode' => $request['zipcode'],
            'description' => $request['description'],
            'created_by' =>  $userid
        ]);
        

        return $leads;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         // validate before update info lead
         $this->validate($request, [
            'first_name' => 'required|string|max:191',
            'last_name' => 'required|string|max:191',
            'user_id' => 'required',
            'source_id' => 'required',
            'credit_report' => 'required',
            'leadstatus_id' => 'required',

        ]);

        $leads = Lead::findOrFail($id);

            $userid = Auth::id();
       
         
            $leads->user_id = $request['user_id'];
            $leads->first_name = $request['first_name'];
            $leads->last_name = $request['last_name'];
            $leads->source_id = $request['source_id'];
            $leads->leadsourcenames = $request['leadsourcenames'];
            $leads->program_id = $request['program_id'];
            $leads->phone = $request['phone'];
            $leads->mobile = $request['mobile'];
            $leads->credit_report = $request['credit_report'];
            $leads->payment20 = $request['payment20'];
            $leads->payment50 = $request['payment50'];
            $leads->leadstatus_id = $request['leadstatus_id'];
            $leads->street = $request['street'];
            $leads->city = $request['city'];
            $leads->state = $request['state'];
            $leads->country = $request['country'];
            $leads->zipcode = $request['zipcode'];
            $leads->description = $request['description'];
           // $leads->created_by =  $userid;
            $leads->save();
        

        return $leads;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     // delete one file   
        $leads = Lead::findOrFail($id);
        $leads->delete();
     

        return ['message' => 'User Deleted'];
    }

    public function deleteall(Request $request)
    {
        // method for delete all data
            for($x=0;$x<count($request->all()); $x++){
             
               $leads = Lead::findOrFail($request->all()[$x]);
               $leads->delete();
            }

            return ['message' => 'User Deleted'];
    }

    public function search(){

        if ($search = \Request::get('q')) {

            if($search == ''){
                $leads = Lead::with(['user','status','createby','source','program'])->latest()->paginate(5);
            }else{
                $leads = Lead::with(['user','status','createby','source','program'])
                            ->lead($search) // lead this from app\Lead.php
                            ->paginate(20);
            }

        }else{
            $leads = Lead::with(['user','status','createby','source','program'])->latest()->paginate(5);
        }
        return $leads;
    }
}
