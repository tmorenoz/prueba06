<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Caffeinated\Shinobi\Models\Role;

use DB;


class RoleController extends Controller
{
    public function index()
    {
        return Role::all();
    }

    public function show(Request $request, $id)
    {
       
                  if($request->ajax()){
                    $role = Role::find($id);
                    $permission =  $role->getPermissions();
                        return response()->json(
                            [
                                'role' => $role,
                                'permission' => $permission,
                            ]
                        );
                  }
          
    }

    public function store(Request $request)
    {
       // find role before create permission

            $findrole = DB::table('permission_role')->where('role_id', $request->get('idprofile'))->get();

            if($findrole->count() > 0)
            {
                // if find role in table permission_role then delete files after create news permission by user
                DB::table('permission_role')->where('role_id', $request->get('idprofile'))->delete();
                
                        for($v=0;$v<count($request->get('arraypermission'));$v++){
                            $createpermission = DB::table('permission_role')->insert(
                                [
                                    'permission_id' => $request->get('arraypermission')[$v],
                                    'role_id' => $request->get('idprofile')
                                ]
                            );
                        }
            }else{
                 // else if dont find role in table permission_role then  create news permission by user
                for($v=0;$v<count($request->get('arraypermission'));$v++){
                    $createpermission = DB::table('permission_role')->insert(
                        [
                            'permission_id' => $request->get('arraypermission')[$v],
                            'role_id' => $request->get('idprofile')
                        ]
                    );
                }

            }

            return ['Profile update rol'];
    }
}
