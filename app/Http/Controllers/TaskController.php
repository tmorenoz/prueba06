<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Task;
use App\Taskpriority;
use App\Taskstatus;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //  method create tasks

        $this->validate($request, [
            'user_id' => 'required',
            'subject' => 'required|string|max:191',
            'lead_id' => 'required',

        ]);

        $tasks = Task::create([
            'user_id' => $request['user_id'],
            'subject' => $request['subject'],
            'date_created' => $request['date_created'],
            'taskstatuses_id' => $request['taskstatuses_id'],
            'taskpriorities_id' => $request['taskpriorities_id'],
            'description' => $request['description'],
            'lead_id' => $request['lead_id']
            
        ]);

        return $tasks;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // list data array with paginate (task)
        return Task::with(['users','lead','status','priority'])->where('lead_id', $id)->paginate(3);
    }

 
    public function getData()
    {
        // method for get two data in components vue
        $taskpriority =  Taskpriority::get();
        $taskstatus = Taskstatus::get();

       return response()->json([
            'taskpriority' => $taskpriority,
            'taskstatus' => $taskstatus
       ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'user_id' => 'required',
            'subject' => 'required|string|max:191',
            'lead_id' => 'required',

        ]);

        $tasks = Task::findOrFail($id);

        $tasks->user_id = $request['user_id'];
        $tasks->subject = $request['subject'];
        $tasks->date_created = $request['date_created'];
        $tasks->taskstatuses_id = $request['taskstatuses_id'];
        $tasks->taskpriorities_id = $request['taskpriorities_id'];
        $tasks->description = $request['description'];
        $tasks->lead_id = $request['lead_id'];
        $tasks->save();
            
        
        return $tasks;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tasks = Task::findOrFail($id);
        if($tasks){
            $tasks->delete();
        }
        return ('Task Delete');
    }
}
